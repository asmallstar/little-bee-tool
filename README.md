# little-bee驱动调测工具

#### 介绍
该项目是开源物联网平台little-bee的驱动调测工具，用于被采集设备驱动的开发、调试，是用QT编写。支持透明传输的TCP客户端网关，串口直连。

#### 界面
![界面](https://gitee.com/asmallstar/little-bee-tool/raw/master/icon/snapshoot.png)


#### 使用方法

1.  选择对应的网关TCP客户端还是直连串口，配置对应的参数
2.  选择开发的驱动，点击加载
3.  查看采集的模拟量，开关量，字符量数据
