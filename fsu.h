#ifndef FSU_H
#define FSU_H

#include <QObject>

class Fsu : public QObject
{
    Q_OBJECT
public:
    explicit Fsu(QObject *parent = nullptr);

    virtual void open(const QString &parameter);
    virtual void write(const QByteArray& data);
    virtual void close();

signals:
    void opened();
    void received(const QByteArray& data);
    void closed();

};

#endif // FSU_H
