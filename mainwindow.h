#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTableWidget>
#include "TCPClient.h"
#include "serialcomm.h"
#include "fsu.h"
#include "driverdll.h"
#include "driver-interface.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

enum next_step_action
{
    NEXT_STEP_ACTION_READ,
    NEXT_STEP_ACTION_WRITE,
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_connectButton_clicked();
    void on_disconnectBtn_clicked();
    void on_openCommBtn_clicked();
    void on_closeCommBtn_clicked();
    void on_driverSelectBtn_clicked();
    void on_driverLoadBtn_clicked();
    void on_driverUnloadBtn_clicked();
    void on_controllerBtn_clicked();

    void opened();
    void closed();
    void readed(const QByteArray& data);

private:
    Ui::MainWindow *ui;
    TCPClient * tcpClient;
    SerialComm * serialComm;
    Fsu * fsu;
    QString dllFileName;
    DriverDll driverDll;
    int timerId;
    driver_context* p_driver_context;
    next_step_action action;

    // QObject interface
protected:
    void timerEvent(QTimerEvent *event);
    void updateChannelValue(response_data_analysis_result *presult);
    void updateTableWidget(QTableWidget * table,driver_channel_value *pvalue,QString value);
};
#endif // MAINWINDOW_H
