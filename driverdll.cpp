#include <QLibrary>
#include "driverdll.h"

DriverDll::DriverDll()
{

}

bool DriverDll::load(QString dllFilename)
{
    QLibrary lib(dllFilename);
    if (lib.load()) {
        fn_new_driver = reinterpret_cast<NEW_DRIVER>(lib.resolve("new_driver"));
        fn_request = reinterpret_cast<REQUEST>(lib.resolve("request"));
        fn_control = reinterpret_cast<CONTROL>(lib.resolve("control"));
        fn_response = reinterpret_cast<RESPONSE>(lib.resolve("response"));
        fn_timeout = reinterpret_cast<TIMEOUT>(lib.resolve("timeout"));
        fn_destroy_driver = reinterpret_cast<DESTROY_DRIVER>(lib.resolve("destroy_driver"));

        return fn_new_driver!=nullptr && fn_request!=nullptr && fn_control!=nullptr &&
               fn_response!=nullptr && fn_timeout!=nullptr && fn_destroy_driver!=nullptr;
    } else
    {
        return false;
    }
}
