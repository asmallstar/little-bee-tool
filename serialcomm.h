#ifndef SERIALCOMM_H
#define SERIALCOMM_H

#include "fsu.h"
#include <QObject>
#include <QSerialPort>

class SerialComm : public Fsu
{
    Q_OBJECT
public:
    explicit SerialComm(QObject *parent = nullptr);

    void open(const QString &parameter) override;
    void write(const QByteArray& data) override;
    void close() override;

private slots:
    void readyRead();

private:
    QSerialPort *serialPort;
};

#endif // SERIALCOMM_H
