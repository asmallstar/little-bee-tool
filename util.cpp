#include "util.h"

Util::Util()
{

}

QString Util::toHexadecimal(const QByteArray &byteArray)
{
    QString str;
    for(int i = 0; i< byteArray.length(); i++){
        QString byteStr = QString::number(static_cast<uchar>(byteArray[i]), 16);
        if(byteStr.length() == 1) str += "0" + byteStr+" ";
        else str += byteStr+" ";
    }
    return str;
}
