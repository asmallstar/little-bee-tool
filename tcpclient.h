#ifndef TCPCLIENT_H
#define TCPCLIENT_H

#include "fsu.h"
#include <QObject>
#include <QTcpSocket>

class TCPClient : public Fsu
{
    Q_OBJECT
public:
    explicit TCPClient(QObject *parent = nullptr);

    void open(const QString &parameter) override;
    void write(const QByteArray& data) override;
    void close() override;


private slots:
    void readyRead();

private:
    QTcpSocket *socket;
};

#endif // TCPCLIENT_H
