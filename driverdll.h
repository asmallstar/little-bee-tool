#ifndef DRIVERDLL_H
#define DRIVERDLL_H

#include <QString>
#include "driver-interface.h"

typedef driver_context* (*NEW_DRIVER)(unsigned char *parameter);
typedef void (*REQUEST)(driver_context* context);
typedef void (*CONTROL)(driver_context* context,driver_control_value * control_value);
typedef void (*RESPONSE)(driver_context* context, response_data_analysis_result* result);
typedef void (*TIMEOUT)(driver_context* context);
typedef void (*DESTROY_DRIVER)(driver_context* context);

class DriverDll
{
public:
    DriverDll();

public:
    bool load(QString dllFilename);

public:
    NEW_DRIVER fn_new_driver;
    REQUEST fn_request;
    CONTROL fn_control;
    RESPONSE fn_response;
    TIMEOUT fn_timeout;
    DESTROY_DRIVER fn_destroy_driver;
};

#endif // DRIVERDLL_H
