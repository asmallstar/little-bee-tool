#include "tcpclient.h"

TCPClient::TCPClient(QObject *parent)
    : Fsu{parent}
{

    socket = new QTcpSocket(this);

    connect(socket, &QTcpSocket::connected, this, &TCPClient::opened);
    connect(socket, &QTcpSocket::readyRead, this, &TCPClient::readyRead);
    connect(socket, &QTcpSocket::disconnected, this, &TCPClient::closed);
}

void TCPClient::open(const QString &parameter)
{
    QStringList list = parameter.split(':');
    socket->connectToHost(list[0], list[1].toUShort());
}

void TCPClient::write(const QByteArray &data)
{
    socket->write(data);
}

void TCPClient::close()
{
    socket->close();
}

void TCPClient::readyRead()
{
    QByteArray data = socket->readAll();
    emit received(data);
}
