#ifndef UTIL_H
#define UTIL_H

#include <QString>

class Util
{
public:
    Util();
public:
    static QString toHexadecimal(const QByteArray &byteArray);
};

#endif // UTIL_H
